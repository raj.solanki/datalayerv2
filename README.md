
![Logo](https://cdn.oneorigin.us/wp-content/uploads/2021/12/OneOrigin-2022-Name-Main.png)


# Data Layer chrome Extension

Data Layer is chrome extension for generating data layer code for ASU



## How To Use Extension

- Download the Zip File
  [Chrome Extension](https://gitlab.com/Jeevan10/datalayer-automation/-/raw/chrome-extension/data-layer-extension.zip?inline=false)

- Extract zip File

#### Step 1 :
Click On three Dots of Chrome
![App Screenshot](/screenshot/1.png)


#### Step 2 :
Go to More settings and then Extensions
![App Screenshot](/screenshot/2.png)


#### Step 3 :
Go to More settings and then Extensions
![App Screenshot](/screenshot/3.png)


#### Step 4 :
Click on Load unpacked and select the extracted file folder
![App Screenshot](/screenshot/4.png)







## 📫 How to reach us...
Developers : 
- [Raj Solanki](mailto:raj.solanki@onerigin.us)